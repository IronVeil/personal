/*
blog.js
Blog router
*/

// --- SETUP ---

// Setup express
const express = require("express")
const router = express.Router()

// Setup filesystem access
const fs = require("fs")

// Setup markdown conversion
const markdownit = require("markdown-it")
const markdown = new markdownit()


// --- GET & POST REQUESTS ---

// Blog homepage
router.get("/", (req, res) => {

    let posts = []
    let files = fs.readdirSync("./public/blogs/") // Get all blog files

    try {
        // Get all titles...
        for (i = 0; i < files.length; i++) {

            // ... read them ...
            let data = fs.readFileSync("./public/blogs/" + files[i], "utf-8").split("\n")[0]
            let post = { title: data, file: files[i].slice(0, -3) }

            // ... and push them to an array
            posts.push(post)
        }
    } catch(err) {

    }

    // Render page
    res.render("blog", { posts: posts })
})

// Blog page
router.get("/:blogpost", (req, res) => {

    // Get post ...
    let data = fs.readFileSync("./public/blogs/" + req.params.blogpost + ".md", "utf-8").split("\n")
    let title = data[0]
    data.shift()
    data.shift()

    // ... convert to HTML ...
    data = data.join("\n")
    let html = markdown.render(data)

    // ... and render
    res.render("blog-post", { title: title, post: html })
})


// --- ROUTER EXPORT ---
module.exports = router