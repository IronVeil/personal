/*
index.js
Entry file for Node
*/

// --- SETUP ---

// Setup express
const express = require("express")
const app = express()

// Setup .env variables
require("dotenv").config()
const PORT = process.env.PORT || 80
const DEBUG = process.env.NODE_ENV || "production"

// Setup pug
app.set("views", "./views")
app.set("view engine", "pug")
app.use(express.static("./public"))


// --- DEBUG ---

// Log key
DEBUG == "dev" && console.log("--- KEY ---\n[S] - Server\n[D] - Debugging GET & POST requests\n--- KEY ---\n")

// Output any requests
app.use((req, res, next) => {
    DEBUG == "dev" && console.log("[D] Requested")
    next()
})


// --- GET & POST REQUESTS ---

// Homepage
app.get("/", (req, res) => {
    res.render("home")
})

// Blog pages
app.use("/blog", require("./routes/blog"))

// Works
app.get("/works", (req, res) => {
    res.render("works")
})

// About
app.get("/about", (req, res) => {
    res.render("about")
})


// --- SERVER ---

// Start
app.listen(PORT, () => {
    DEBUG == "dev" && console.log("[S] Server started on port " + PORT)
})